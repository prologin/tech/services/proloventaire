# noqa
# pylint: skip-file
"""
Django settings for proloventaire project.

For more information on this file, see
https://docs.djangoproject.com/en/4.0/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/4.0/ref/settings/
"""

from pathlib import Path

import django
from django.contrib.messages import constants as messages
from django_utils.settings.caches import *
from django_utils.settings.common import *
from django_utils.settings.databases import *
from django_utils.settings.logging import *

from django_utils.settings.auth import *  # isort:skip
from django_utils.settings.celery import *  # isort:skip
from django_utils.settings.drf import *  # isort:skip


PROJECT_NAME = "proloventaire"

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent


# Application definition

INSTALLED_APPS = installed_apps(with_auth=True) + [
    "auditlog",
    "django.forms",
    "localflavor",
]

FORM_RENDERER = "django.forms.renderers.TemplatesSetting"

MIDDLEWARE = middleware(with_auth=True) + [
    "auditlog.middleware.AuditlogMiddleware",
]

DATABASES = databases(PROJECT_NAME)
DATABASES["default"]["ATOMIC_REQUESTS"] = True

ROOT_URLCONF = root_urlconf(PROJECT_NAME)

WSGI_APPLICATION = wsgi_application(PROJECT_NAME)

REST_FRAMEWORK = rest_framework(with_auth=True)

OIDC_SYNC_GIVEN_NAME = True

MESSAGE_TAGS = {
    messages.DEBUG: "info",
    messages.INFO: "info",
    messages.SUCCESS: "hint",
    messages.WARNING: "warn",
    messages.ERROR: "warn",
}


# Project specific settings

DOCUMENTS_GENERATOR_EXPENSE_REPORT_ENDPOINT = env.get_string(
    "DOCUMENTS_GENERATOR_EXPENSE_REPORT_ENDPOINT", ""
)

# Email to notify when expense reports need managing
EXPENSE_REPORTS_MANAGERS = env.get_list("EXPENSE_REPORTS_MANAGERS", [])
